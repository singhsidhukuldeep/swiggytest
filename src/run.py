try:
    from parkingLot import parkingLot
    import os.path
except Exception as e: 
    print ("ERROR in importing")
    print (e)

def create(userInput):
    try:
        N = int(userInput.split(" ")[-1])
        myParkingLot = parkingLot(N)
        return myParkingLot
    except Exception as e: 
        print ("ERROR in create")
        print (e)

def park (myParkingLot, userInput):
    try:
        carNumber = userInput.split(" ")[1]
        carColor = userInput.split(" ")[-1]
        myParkingLot.park(carNumber, carColor)
        return myParkingLot
    except Exception as e: 
        print ("ERROR in park")
        print (e)

def leave (myParkingLot, userInput):
    try:
        try:
            slotNumber = int(userInput.split(" ")[-1])
            myParkingLot.remove (slotNumber)
        except Exception as e: 
            print ("ERROR in leave")
            print (e)
        return myParkingLot
    except Exception as e: 
        print ("ERROR in leave")
        print (e)

def show (myParkingLot, userInput):
    try:
        filter = userInput.split(" ")[-1]
        filterL = []
        if filter == "allocated":
            filterL.append("Is Parked")
            filterL.append(True)
        elif filter == "free":
            filterL.append("Is Parked")
            filterL.append(False)
        else:
            print ("Unknown filter")
        myParkingLot.showFilteredLot(filterL)
        return myParkingLot
    except Exception as e: 
        print ("ERROR in show")
        print (e)

def runner (userInput, myParkingLot):
    try:
        if ("create_parking_lot" in userInput):
                myParkingLot = create(userInput)
                return myParkingLot

        elif ("park " in userInput):
            myParkingLot = park(myParkingLot, userInput)
            return myParkingLot

        elif ("leave " in userInput):
            myParkingLot = leave(myParkingLot, userInput)
            return myParkingLot
        
        elif ("status " in userInput):
            myParkingLot = show (myParkingLot, userInput)
            return myParkingLot
        
        else:
            print ("Not a query")
            return myParkingLot
    except Exception as e: 
        print ("ERROR in runner")
        print (e)

def __main__():
    try:
        userInput = input()
        myParkingLot = None
        
        if (os.path.isfile(userInput)):
            f = open(userInput)
            for i in f.readlines():
                userInput = i.strip().replace("\n","")
                myParkingLot = runner (userInput, myParkingLot)
        else :
            userInput = userInput.strip().replace("\n","")
            myParkingLot = runner (userInput, myParkingLot)
            print ("Input 'exit' to close program")
            while (True):
                userInput = input()
                if userInput.lower() == "exit":
                    break
                userInput = userinput.strip().replace("\n","")
                myParkingLot = runner (userInput, myParkingLot)
    except Exception as e: 
        print ("ERROR in main")
        print (e)
            

__main__()
