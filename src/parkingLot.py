class parkingLot:
    
    pl = []
    ns = 0
    
    def __init__(self, numberOfSlots):
        try:
            self.numberOfSlots = numberOfSlots
            for i in range(numberOfSlots):
                parkingLot.pl.append({"Slot Number":i+1,
                "Is Parked": False,
                "Car Number":"",
                "Car Color":""})
            print ("Created a parking lot with", numberOfSlots, "slots")
        except Exception as e: 
            print ("ERROR in ParkingLot")
            print (e)
    
    def park (self, carNumber, carColor):
        try:
            if parkingLot.ns <= len(parkingLot.pl):
                i = parkingLot.pl[parkingLot.ns]
                if i["Is Parked"] == False:
                        i["Is Parked"] = True
                        i["Car Number"] = carNumber
                        i["Car Color"] = carColor
                        print ("Allocated slot number:", i["Slot Number"])
                        parkingLot.ns += 1
                        return
            for i in parkingLot.pl:
                if i["Is Parked"] == False:
                    i["Is Parked"] = True
                    i["Car Number"] = carNumber
                    i["Car Color"] = carColor
                    print ("Allocated slot number:", i["Slot Number"])
                    parkingLot.ns += 1
                    return
            print ("Parking lot is full")
        except Exception as e: 
            print ("ERROR in ParkingLot")
            print (e)

    
    def remove (self, slotNumber):
        try:
            if (parkingLot.pl[slotNumber-1]["Is Parked"] == True and slotNumber-1 <= len(parkingLot.pl)):
                parkingLot.pl[slotNumber-1]["Is Parked"] = False
                parkingLot.pl[slotNumber-1]["Car Number"] = ""
                parkingLot.pl[slotNumber-1]["Car Color"] = ""
                print ("Slot number", slotNumber, "is freed")
                parkingLot.ns = min(parkingLot.ns, slotNumber-1)
            else:
                print (slotNumber, "Lot is empty")
        except Exception as e: 
            print ("ERROR in ParkingLot")
            print (e)
    
    def showLot (self):
        try:
            print ("Slot No \tRegistration \tColor")
            for i in parkingLot.pl:
                print (str(i["Slot Number"]) + "\t" + str(i["Car Number"]) + "\t" + str(i["Car Color"]))
        except Exception as e: 
            print ("ERROR in ParkingLot")
            print (e)
    
    def showFilteredLot (self, filter):
        try:
            print ("Slot No \tRegistration \tColor")
            for i in parkingLot.pl:
                if (i[filter[0]] == filter[-1]):
                    print (str(i["Slot Number"]) + "\t" + str(i["Car Number"]) + "\t" + str(i["Car Color"]))
        except Exception as e: 
            print ("ERROR in ParkingLot")
            print (e)

